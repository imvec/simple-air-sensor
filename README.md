# SIMPLE AIR SENSOR

https://publiclab.org/wiki/simple-air-sensor

Simple Air Sensor es un prototipo de sensor de partículas solidas en suspensión desarrollado por la comunidad Publiclab y basado en Arduino y el sensor de calidad de aire Plantower 5003.

Los archivos .ino presentes en esta carpeta incorporan variaciones al código original plantower-rgb-webjack.ino desarrollado por Jeff Warren.

**simple-air-openlog.ino**: incorpora el datalogger Openlog para almacenar los datos en una tarjeta microSD..

**simple-air-openlog-csv.ino**: reorganiza los datos recopilados en el Openlog en archivo CSV.

simple-air-openlog-csv-rtc.ino: añade Reloj de Tiempo Real para poner fecha y hora a las mediciones.

**simple-air-openlog-csv-rtc-bme280.ino**: incorpora a lo anterior el sensor BME280 de temperatura, humedad y presión atmosférica. Las mediciones se realizan cada 60 segundos.

**simple-air-openlog-csv-rtc-bme280-oled.ino**: las lecturas de los sensores se muestran en tiempo real en una pantalla oled.

**simple-air-openlog-csv-rtc-bme280-oled-multichannel-gas-semsor.ino**: añade el sensor multicanal de seeedstudio.

Descarga el archivo "[librerias.zip](https://gitlab.com/imvec/simple-air-sensor/-/raw/master/librerias.zip?inline=false)" para obtener las librerías necesarias.

## Enlaces de interés

Assembling the Simple Air Sensor: https://publiclab.org/notes/warren/03-19-2019/assembling-the-simple-air-sensor

Record data from a Simple Air Sensor: https://publiclab.org/notes/warren/09-10-2019/record-data-from-a-simple-air-sensor

How to open and clean a Simple Air PMS5003 Sensor: https://publiclab.org/notes/kkoerner/09-17-2018/how-to-open-and-clean-a-pms-5003-air-sensor

Log Simple Air Sensor sensor data to SD card: https://publiclab.org/notes/imvec/12-17-2019/logging-simple-air-sensor-data-to-sd-card

 

