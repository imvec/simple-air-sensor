//FORKED by imvec.tech from // https://gist.github.com/jywarren/d9cdc98a4a50d8505ad04521e45ce59d

#include <SD.h>
#include <Wire.h>
#include "RTClib.h"
#include <SPI.h>
#include "Seeed_BME280.h"
#include "MutichannelGasSensor.h"
#include <SeeedOLED.h>

// En placas Leonardo/Micro y otras con hardware serial usa este
// descomenta esta linea:
// #define pmsSerial Serial1

// Para placas UNO/Nano y otras sin hardware serial, debemos usar software serial...
// pin #2 es IN del sensor Plantower 5003 (TX en el sensor), deja el pin #3 desconectado
// comenta estas dos lineas si estás usando hardware serial
#include <SoftwareSerial.h>
SoftwareSerial pmsSerial(2, 3);

int redPin   = 4; // d4
int greenPin = 5; // d5
int bluePin  = 7; // d7

#include <SoftModem.h>
SoftModem modem = SoftModem();

BME280 bme280;                               // Sensor i2c de temperatura, humedad y presión atmosférica.
RTC_DS1307 rtc;                             //  Reloj i2c de tiempo real

void setup() {
  rtc.begin();                              // Inicializa el reloj de tiempo real
  Wire.begin();
  SD.begin();                               // Inicializa el Openlog
  Serial.begin(115200);                     // Inicializa el serial monitor (cambia los baudios a 115200)
  gas.begin(0x04);                          // Inicializa el sensor de gases
  gas.powerOn();                            // Inicializa el sensor de gases
  pmsSerial.begin(9600);                    // Inicializa el sensor Plantower 5003
  bme280.init();                            // Inicializa el sensor bme280
  SeeedOled.init();                         // Inicializa la pantalla OLED i2c

  // rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));     // Ponemos en hora, solo la primera vez, luego comentar esta linea y volver a cargar el sketch

  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);

  // inicializa softmodem
  delay(100);
  modem.begin();
}

struct pms5003data {
  uint16_t framelen;
  uint16_t pm10_standard, pm25_standard, pm100_standard;
  uint16_t pm10_env, pm25_env, pm100_env;
  uint16_t particles_03um, particles_05um, particles_10um, particles_25um, particles_50um, particles_100um;
  uint16_t unused;
  uint16_t checksum;
};

struct pms5003data data;

void loop() {
    float pressure;
    float c;
    DateTime now = rtc.now();
    if (readPMSdata(&pmsSerial)) {
    // reading data was successful!
    Serial.println();
    Serial.print(now.year());
    Serial.print('/');
    Serial.print(now.month());
    Serial.print('/');
    Serial.print(now.day());
    Serial.print(" ");
    Serial.print(now.hour());
    Serial.print(':');
    Serial.print(now.minute());
    Serial.print(':');
    Serial.print(now.second());
    Serial.print(",");
    Serial.print(bme280.getTemperature());
    Serial.print(',');
    Serial.print(bme280.getHumidity());
    Serial.print(',');
    Serial.print(bme280.getPressure());
    Serial.print(',');
    c = gas.measure_NO2();
    if(c>=0) Serial.print(c);
    Serial.print(",");
    c = gas.measure_CO();
    if(c>=0) Serial.print(c);
    Serial.print(",");
    Serial.print(data.pm10_standard);
    Serial.print(",");
    Serial.print(data.pm25_standard);
    Serial.print(",");
    Serial.print(data.pm100_standard);
    Serial.print(",");
    Serial.print(data.pm10_env);
    Serial.print(",");
    Serial.print(data.pm25_env);
    Serial.print(",");
    Serial.print(data.pm100_env);
    Serial.print(",");
    Serial.print(data.particles_03um);
    Serial.print(",");
    Serial.print(data.particles_05um);
    Serial.print(",");
    Serial.print(data.particles_10um);
    Serial.print(",");
    Serial.print(data.particles_25um);
    Serial.print(",");
    Serial.print(data.particles_50um);
    Serial.print(",");
    Serial.print(data.particles_100um);
    Serial.print(",");
    SeeedOled.clearDisplay();                         
    SeeedOled.setNormalDisplay();                    
    SeeedOled.setPageMode();
    SeeedOled.setTextXY(1,0);
    SeeedOled.putString ("PM1          ");
    SeeedOled.putNumber (data.pm25_env);
    SeeedOled.setTextXY(2,0);
    SeeedOled.putString ("PM2.5        ");
    SeeedOled.putNumber (data.pm25_env);
    SeeedOled.setTextXY(3,0);
    SeeedOled.putString ("PM10         ");
    SeeedOled.putNumber (data.pm100_env);                     
    SeeedOled.setTextXY(4,0);
    SeeedOled.putString ("");
    SeeedOled.setTextXY(5,0);
    SeeedOled.putString ("Temperatura   ");
    SeeedOled.putNumber (bme280.getTemperature());
    SeeedOled.setTextXY(6,0);
    SeeedOled.putString ("Humedad       ");
    SeeedOled.putNumber (bme280.getHumidity());
    SeeedOled.setTextXY(7,0);                        
    SeeedOled.putString ("Monoxido CO   ");
    SeeedOled.putNumber (gas.measure_CO());
       
    int minVal = 0;
    int maxVal = 20; // arbitrary
    // map values to a portion of the 360 degree color wheel
    int hue = max(0, min(359, map(data.pm25_standard, minVal, maxVal, 140, 0)));
    setLedColorHSV(hue, 1, 1);
    delay(5000);

  }
}

boolean readPMSdata(Stream *s) {
  if (! s->available()) {
    return false;
  }

  // Read a byte at a time until we get to the special '0x42' start-byte
  if (s->peek() != 0x42) {
    s->read();
    return false;
  }

  // Now read all 32 bytes
  if (s->available() < 32) {
    return false;
  }

  uint8_t buffer[32];
  uint16_t sum = 0;
  s->readBytes(buffer, 32);

  // get checksum ready
  for (uint8_t i = 0; i < 30; i++) {
    sum += buffer[i];
  }

  /* debugging
    for (uint8_t i=2; i<32; i++) {
    Serial.print("0x"); Serial.print(buffer[i], HEX); Serial.print(", ");
    }
    Serial.println();
  */

  // The data comes in endian'd, this solves it so it works on all platforms
  uint16_t buffer_u16[15];
  for (uint8_t i = 0; i < 15; i++) {
    buffer_u16[i] = buffer[2 + i * 2 + 1];
    buffer_u16[i] += (buffer[2 + i * 2] << 8);
  }

  // put it into a nice struct :)
  memcpy((void *)&data, (void *)buffer_u16, 30);

  if (sum != data.checksum) {
    Serial.println("");
    return false;
  }
  // success!
  return true;
}




// http://www.hobbitsandhobos.com/wp-content/uploads/2011/06/colorWheel.png
// Convert a given HSV (Hue Saturation Value) to RGB (Red Green Blue)
// and set the led to the color
//  h is hue value, integer between 0 and 360
//  s is saturation value, double between 0 and 1
//  v is value, double between 0 and 1
// http://splinter.com.au/blog/?p=29
void setLedColorHSV(int h, double s, double v) {

  double r = 0;
  double g = 0;
  double b = 0;

  double hf = h / 60.0;

  int i = (int)floor(h / 60.0);
  double f = h / 60.0 - i;
  double pv = v * (1 - s);
  double qv = v * (1 - s * f);
  double tv = v * (1 - s * (1 - f));

  switch (i)
  {
    case 0: // rojo dominante
      r = v;
      g = tv;
      b = pv;
      break;
    case 1: // verde
      r = qv;
      g = v;
      b = pv;
      break;
    case 2:
      r = pv;
      g = v;
      b = tv;
      break;
    case 3: // azul
      r = pv;
      g = qv;
      b = v;
      break;
    case 4:
      r = tv;
      g = pv;
      b = v;
      break;
    case 5: // rojo
      r = v;
      g = pv;
      b = qv;
      break;
  }

  // set each component to a integer value between 0 and 255
  int red = constrain(255 - (int)255 * r, 0, 255);
  int green = constrain(255 - (int)255 * g, 0, 255);
  int blue = constrain(255 - (int)255 * b, 0, 255);

  setLedColor(red, green, blue);
}

void setLedColor(int red, int green, int blue) {
  analogWrite(redPin,   red);
  analogWrite(greenPin, green);
  analogWrite(bluePin,  blue);
}
